import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Import to load these templates
import '../../ui/pages/welcomepage.js';
import '../../ui/pages/scandevice.js';
import '../../ui/pages/settingwifi.js';
import '../../ui/pages/homepage.js';



FlowRouter.route('/', {
    name: 'home',
    action() {
        var localStorage = window.localStorage;
        console.log("ip:" + localStorage.getItem("device_ip") );
        console.log("ip:" + (localStorage.getItem("device_ip") === null));
        console.log("ip:" + (localStorage.getItem("device_ip") === undefined));
        if(localStorage.getItem("device_ip") == '' || localStorage.getItem("device_ip") === null || localStorage.getItem("device_ip")=== undefined){
            console.log("11111111");
            FlowRouter.go('welcomepage');
        }else{
            console.log("22222222");
            FlowRouter.go('homepage');
        }
    },
});

FlowRouter.route('/homepage', {
    name: 'homepage',
    action() {
        BlazeLayout.render('layout', { main: 'homepage'});
    }
});

FlowRouter.route('/blelists', {
    name: 'scandevice',
    action() {
        BlazeLayout.render('layout', { main: 'scandevice'});
    }
});

FlowRouter.route('/settingwifi', {
    name: 'settingwifi',
    action() {
        BlazeLayout.render('layout', { main: 'settingwifi'});
    }
});

FlowRouter.route('/login', {
    name: 'loginpage',
    action() {
        BlazeLayout.render('layout', { main: 'homepage'});
    }
});

FlowRouter.route('/welcomepage', {
    name: 'welcomepage',
    action() {
        BlazeLayout.render('layout', { main: 'welcomepage'});
    }
});