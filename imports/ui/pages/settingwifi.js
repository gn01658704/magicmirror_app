import {Template} from 'meteor/templating'
import {EJSON} from 'meteor/ejson'
import './SettingWifi.html';

var Devices = new Array();



// networks = [
//     {   "level": signal_level, // raw RSSI value
//         "SSID": ssid, // SSID as string, with escaped double quotes: "\"ssid name\""
//         "BSSID": bssid // MAC address of WiFi router as string
//         "frequency": frequency of the access point channel in MHz
//         "capabilities": capabilities // Describes the authentication, key management, and encryption schemes supported by the access point.
//         "timestamp": timestamp // timestamp of when the scan was completed
//         "channelWidth":
//             "centerFreq0":
// "centerFreq1":
// }
// ]


Template.settingwifi.onCreated(function wifipageOnCreated() {
    console.log('wifipage');
    Session.set("wifis",'');
    if (Meteor.isCordova) {
        WifiWizard2.scan().then( function( results ){
            console.log( 'Found networks:' + EJSON.stringify(results));
            Session.set("wifis",results);
        }).catch( function( error ){
            console.log( 'Error getting results!', error );
        });

        var device = Session.get("connect_device");
        ble.startNotification(device.id, 'ec00', 'ffffffff-ffff-ffff-ffff-fffffffffff1', onNotify, onError);
    }
});

Template.settingwifi.events({
    "click #setwifi": function (event, template) {
        console.log('setting wifi:');
        var password = template.find("#passwd").value;  //密碼
        var ssid = template.find("#select_wifi").value;  //SSID
        console.log('password:' + password);
        console.log('ssid:' + ssid );

        var Wifi = {};
        Wifi.SSID = ssid;
        Wifi.PASWD = password;
        //Session.set("dailog",{title:"Wifi setting", content:"Setting...", bottom: false});
        //$('#settingModal').modal('show');
        //if (Meteor.isCordova) {

        //}

        var device = Session.get("connect_device");
        console.log('device:' + EJSON.stringify(device));
        ble.write(device.id, 'ec00', 'ffffffff-ffff-ffff-ffff-fffffffffff1', stringToBytes(EJSON.stringify(Wifi)),
            function(data) {
                console.log('success:' + EJSON.stringify(data));
                Session.set("dailog",{title:"Wifi setting", content:"Setting...", bottom: false});
                $('#settingModal').modal('show');
            },function(failure){
                console.log('fail:'/* + EJSON.stringify(state)*/);
            });
    },
    'change #select_wifi': function (e) {
        console.log('value:', $(e.target).val());
    }
});


Template.settingwifi.helpers({
    showWifis: function () {
        return Session.get("wifis");
    },
    showDailog: function () {
        return Session.get("dailog");
    }
});



function onDiscoverDevice(device) {
    console.log('Found Device:' + /*EJSON.stringify(device)*/ device.name);
    Devices.push(device);
    Session.set("device", Devices);
}

function onError(state) {
    console.log('onError:' + state);
}

function onNotify(buffer) {
    console.log('buffer:' + bytesToString(buffer));
    var code =  EJSON.parse(bytesToString(buffer));

    if(code == 0){
        $('#settingModal').modal('hide');
        // $('#settingModal').modal('dispose');

        console.log("get data:");
        var device = Session.get("connect_device");
        ble.read(device.id, 'ec00', 'ffffffff-ffff-ffff-ffff-fffffffffff1',
            function(data){
                console.log("Hooray we have data:"+ bytesToString(data));
                var wifi =  EJSON.parse(bytesToString(data));
                var localStorage = window.localStorage;
                localStorage.setItem("device_ip", wifi.ip);
                Session.set("wifi", wifi);
                ///////FlowRouter.go('loginpage');
            },
            function(failure){
                var localStorage = window.localStorage;
                localStorage.setItem("device_ip", null);
                console.log(failure);
            }
        );

    }else {
        console.log('===> FAIL');
        //$('#settingModal').modal('hide');
        if(code == 1){
            Session.set("dailog",{title:"Wifi setting", content:"WIFI設定失敗,請檢查WIFI密碼", bottom: true});
        }else if(code == 2){
            Session.set("dailog",{title:"Wifi setting", content:"WIFI設定失敗,請檢查WIFI密碼", bottom: true});
        }
        //$('#settingModal').modal('show');
    }

}

// ASCII only
function stringToBytes(string) {
    var array = new Uint8Array(string.length);
    for (var i = 0, l = string.length; i < l; i++) {
        array[i] = string.charCodeAt(i);
    }
    return array.buffer;
}

// ASCII only
function bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
}

