import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';
import {ServiceConfiguration} from 'meteor/service-configuration';
import {EJSON} from 'meteor/ejson'

import './WelcomePage.html';

Template.welcomepage.onCreated(function helloOnCreated() {
    // counter starts at 0
    this.counter = new ReactiveVar(0);
});

Template.welcomepage.helpers({
    counter() {
        return Template.instance().counter.get();
    },
});


Template.welcomepage.events({
    'click #start_scan'(event, instance) {
        //console.log(Meteor.user())

        console.log( 'click button');
        FlowRouter.go('scandevice');
        //FlowRouter.go('settingwifi');
        // FlowRouter.go('loginpage');

        //const {scope} = ServiceConfiguration.configurations.findOne({service: 'google'});

        // {
        //     //headers:{'Content-Type':'application/json;charset=utf-8'},
        //     query: {
        //         's':'wifi'
        //     }
        // }


        /*
        window.plugins.googleplus.login(
            {
                'scopes': 'https://mail.google.com/' +
                    //' https://www.googleapis.com/auth/userinfo.profile' +
                    //' https://www.googleapis.com/auth/userinfo.email' +
                    ' https://www.googleapis.com/auth/admin.directory.resource.calendar', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
                'webClientId': '552642140296-acdbgm7qkn49kn2j8nub0tharf1eoiv4.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
                'offline': true // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
            },
            function (obj) {
                console.log(JSON.stringify(obj)); //If there is any error, will get error here
            },
            function (msg) {
                alert('error: ' + msg);
            }
        );*/
        /*
        Meteor.loginWithGoogle({
            requestPermissions: scope,
            redirect_uri: 'http://localhost:3000/callback/google',
            requestOfflineToken: { google: true }
    }, function (error) {
            if (error) {
                console.log(error); //If there is any error, will get error here
            } else {
                console.log("login successful");// If there is successful login, you will get login details here
                console.log(EJSON.stringify(Meteor.user()));// If there is successful login, you will get login details here
            }
        });*/
    },
    'click #scan_qrcode': function (event, template) {

        cordova.plugins.barcodeScanner.scan(
            function (result) {
                /*
                alert("We got a barcode\n" +
                    "Result: " + result.text + "\n" +
                    "Format: " + result.format + "\n" +
                    "Cancelled: " + result.cancelled);*/
                var strs = result.text.match('http://(.*?)/');
                if(strs.length > 0){
                    var ip = strs[1];
                    var localStorage = window.localStorage;
                    localStorage.setItem("device_ip",ip.replace(":8080",""));
                    FlowRouter.go('/');
                }
            },
            function (error) {
                //alert("Scanning failed: " + error);
            },
            {
                preferFrontCamera: false, // iOS and Android
                showFlipCameraButton: true, // iOS and Android
                showTorchButton: true, // iOS and Android
                torchOn: true, // Android, launch with the torch switched on (if available)
                saveHistory: false, // Android, save scan history (default false)
                prompt: "Place a qrcode inside the scan area", // Android
                resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                formats: "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
                orientation: "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
                disableAnimations: true, // iOS
                disableSuccessBeep: false // iOS and Android
            }
        );
    }
});
// given the url: "/post/5?q=s#hashFrag"

