import {Template} from 'meteor/templating'
import {EJSON} from 'meteor/ejson'
import './ScanDevice.html';

var Devices = new Array();


Template.scandevice.onCreated(function scandeviceOnCreated() {
    console.log('scandevice');
    if (Meteor.isCordova) {
        Session.set("device", "");
        ble.isEnabled(
            function () {
                console.log("Bluetooth is enabled");
                Devices = [];
                ble.scan([/*'ffffffff-ffff-ffff-ffff-fffffffffff0'*/], 10, onDiscoverDevice, onError);
            },
            function () {
                console.log("Bluetooth is *not* enabled");
            }
        );

    }
});


Template.scandevice.events({
    "click #connect_bt": function (event, template) {
        var device_id = $('#SelectMirror').val();
        var device;
        //$('#entercodeModal').modal('show');

        Devices.forEach((value, index, ar) => {
            if(value.id === device_id){
                device = value;
            }
        });

        console.log('device_id:' + device_id);
        console.log('Devices:' + EJSON.stringify(Devices));
        console.log('device:' + EJSON.stringify(device));
        if (device != null) {
            Session.set("connect_device", device);
            ble.stopScan(
                function () {
                    console.log("Scan complete");
                },
                function () {
                    console.log("stopScan failed");
                }
            );
            ble.connect(device.id, connectCallback, disconnectCallback);
        }
    },
    "hidden.bs.modal #entercodeModal": function (event, template) {
        console.log('hidden:');
        var password = template.find("#invitationcode").value;  //密碼
        console.log('code:' + password);
        var device = Session.get("connect_device");
        ble.write(device.id, 'ec00', 'ffffffff-ffff-ffff-ffff-fffffffffff2', stringToBytes("" + password),
            function(data) {
                console.log('success:' + EJSON.stringify(data));
                $('#entercodeModal').modal('hide');
            },function(failure){
                console.log('fail:'/* + EJSON.stringify(state)*/);
            });
    },
    "change #SelectMirror": function(event, template) {
        console.log('SelectMirror:' + EJSON.stringify($(event.target).val()));
        var device = $(event.target).val();
        console.log('connect:' + device);
        Session.set("select_device", device);
    }
});


Template.scandevice.helpers({
    counter() {
        return Template.instance().counter.get();
    },
    showDevices: function () {
        return Session.get("device");
    }
});

function connectCallback(state) {
    console.log('connectCallback:' + EJSON.stringify(state));
    var device = Session.get("connect_device");
    //ble.startNotification(device.id, 'ec00', 'ffffffff-ffff-ffff-ffff-fffffffffff2', onNotify, onError);
    //$('#entercodeModal').modal('show');

    //暫時修改
    var localStorage = window.localStorage;
    localStorage.setItem("device", EJSON.stringify(Session.get("connect_device")));
    FlowRouter.go('settingwifi');
}


function onDiscoverDevice(device) {
    console.log('Found Device:' + EJSON.stringify(device));

    //console.log('Found Device:' + /*EJSON.stringify(device)*/ buf2hex(device.advertising));

    Devices.push(device);
    Session.set("device", Devices);

    // if(device.name == 'McubeMirror'){
    //     Devices.push(device);
    //     Session.set("device", Devices);
    // }
}

function onError(state) {
    console.log('onError:' + state);
}


function onNotify(buffer) {
    console.log('buffer:' + bytesToString(buffer));
    var code =  EJSON.parse(bytesToString(buffer));

    if(code == 0){
        var localStorage = window.localStorage;
        localStorage.setItem("device", EJSON.stringify(Session.get("connect_device")));
        FlowRouter.go('settingwifi');
    }else {
        if(code == 1){
            Session.set("dailog",{title:"PIN Code", content:"PIN Code 輸入錯誤", bottom: true});
        }
        //$('#settingModal').modal('show');
    }
}


function disconnectCallback(state) {
    console.log('disconnectCallback:' + EJSON.stringify(state));
}

// ASCII only
function stringToBytes(string) {
    var array = new Uint8Array(string.length);
    for (var i = 0, l = string.length; i < l; i++) {
        array[i] = string.charCodeAt(i);
    }
    return array.buffer;
}

// ASCII only
function bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
}

function buf2hex(buffer) { // buffer is an ArrayBuffer
    return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}