import {Template} from 'meteor/templating'
import {EJSON} from 'meteor/ejson'
import './homepage.html';

var wifi_ip = null;


Template.homepage.onCreated(function loginpageOnCreated() {
    var localStorage = window.localStorage;
    wifi_ip = localStorage.getItem("device_ip");

    var httpstring = 'http://' + wifi_ip + ':8080/news';
    HTTP.get(httpstring, {timeout: 2000}, function (error, response) {
        if (error) {
            console.log(error);
            $('#connect_dialog').modal('show');
            Session.set("connect_dialog", {
                title: "連接Mirror失敗",
                content: "連線失敗,要重新設定Mirror嗎?",
                bottom: true
            });
        } else {
            console.log(response);
            //var photolist = EJSON.parse(response.content);
            Session.set("news", response.content);
        }
    });

});

Template.homepage.events({
    'click #google_login': function (event, template) {
        window.plugins.googleplus.login(
            {
                'scopes': 'https://mail.google.com/' +
                    ' https://www.googleapis.com/auth/userinfo.profile' +
                    ' https://www.googleapis.com/auth/userinfo.email' +
                    ' https://www.googleapis.com/auth/calendar'
                , // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
                'webClientId': '552642140296-acdbgm7qkn49kn2j8nub0tharf1eoiv4.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
                'offline': true, // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
                'force': true
            },
            function (obj) {

                //var mywifi = Session.get("wifi");

                console.log(JSON.stringify(obj)); //If there is any error, will get error here
                //console.log("mywifi:" +  JSON.stringify(mywifi)); //If there is any error, will get error here
                var postData = {
                    data: obj
                };
                var httpstring = 'http://' + wifi_ip + ':8080/post?s=email';
                console.log(httpstring);
                HTTP.post(httpstring, postData, function (error, response) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log(response);
                    }
                });
            },
            function (msg) {
                alert('error: ' + msg);
            }
        );
    },
    'click #show_photo': function (event, template) {
        var httpstring = 'http://' + wifi_ip + ':8080/photolist';
        console.log(httpstring);
        HTTP.get(httpstring, function (error, response) {
            if (error) {
                console.log(error);
            } else {
                console.log(response);
                var photolist = EJSON.parse(response.content);

                photolist.forEach((value, index, ar) => {
                    value.fullpath = 'http://' + wifi_ip + ':8080/' + value.path;
                });
                Session.set("photolist", photolist);
                $('#photos').modal('show');

            }
        });
    },
    'click #delete_photo': function (event, template) {
        console.log(this);

        var Data = {
            data: this
        };

        var httpstring = 'http://' + wifi_ip + ':8080/delete?s=img';
        HTTP.del(httpstring, Data, function (error, response) {
            if (error) {
                console.log(error);
            } else {
                console.log(response);
                var photolist = EJSON.parse(response.content);
                photolist.forEach((value, index, ar) => {
                    value.fullpath = 'http://' + wifi_ip + ':8080/' + value.path;
                });
                Session.set("photolist", photolist);
            }
        })

    },
    'click #upload_photo': function (event, template) {
        $("#im_file").click();
    },
    'change #im_file': function (event, template) {
        var httpstring = 'http://' + wifi_ip + ':8080/imageUpload';
        $.ajaxFileUpload(
            {
                url: httpstring,
                secureuri: false,
                fileElementId: 'im_file',
                dataType: 'json',
                success: function (data, status) {
                    console.log("success:", data);
                    getPhoto();
                },
                error: function (data, status, e) {
                    console.log(e);
                    getPhoto();
                }
            }
        )
    },
    'click #scan_qrcode': function (event, template) {
        FlowRouter.go('welcomepage');
    },
    'click #exit': function (event, template) {
        console.log(this);
        var localStorage = window.localStorage;
        localStorage.setItem("device_ip", '');
        FlowRouter.go('/');
    }
});


Template.homepage.helpers({
    photolist: function () {
        return Session.get("photolist");
    },
    checkedNews: function (item) {
        console.log("checkedNews", Session.get("news"));
        switch (item) {
            case 1:
                return Session.get("news") === "CNN";
            case 2:
                return Session.get("news") === "BBC";
        }
    },
    showDailog: function () {
        return Session.get("connect_dialog");
    }
});

function getPhoto() {
    var httpstring = 'http://' + wifi_ip + ':8080/photolist';
    console.log(httpstring);
    HTTP.get(httpstring, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log(response);
            var photolist = EJSON.parse(response.content);

            photolist.forEach((value, index, ar) => {
                value.fullpath = 'http://' + wifi_ip + ':8080/' + value.path;
            });
            Session.set("photolist", photolist);
            $('#photos').modal('show');

        }
    });
}