(function () {

    myTimer();
    var myVar = setInterval(myTimer, 10000);

    function myTimer() {
        var fullDate = new Date();
        var m = fullDate.getMonth();
        var d = fullDate.getDate();
        var h = fullDate.getHours();
        var min = fullDate.getMinutes();
        if (fullDate.getMinutes() < 10) {
            var min = "0" + fullDate.getMinutes();
        }
        if (fullDate.getHours() < 10) {
            var h = "0" + fullDate.getHours();
        }
        var weekday = new Array(7);
        weekday[0] = "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";
        var day = weekday[fullDate.getDay()];

        document.getElementById("Month").innerHTML = m + 1;
        document.getElementById("Date").innerHTML = d;
        document.getElementById("Hour").innerHTML = h;
        document.getElementById("Min").innerHTML = min;
        document.getElementById("Day").innerHTML = day;

    }
})();